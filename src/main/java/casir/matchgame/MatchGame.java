package casir.matchgame;

import java.lang.Exception;
import java.util.Scanner;
import java.util.Random;

public class MatchGame {

    private int remaining;

    public static void main(String[] args){
        MatchGame match = new MatchGame(10);
        match.play();
    }

    public MatchGame(int remaining) {
        this.remaining = remaining;
    }

    public void play() {
        System.out.println("----------- Nouveau tour -----------");
        while(this.step()) {
            System.out.println();
            System.out.println("----------- Nouveau tour -----------");
        }
    }

    private boolean step() {
        this.playerPick();
        if(this.isFinished()) {
            System.out.println("Tu as gagné !");
            return false;
        }
        this.displayRemaining();
        this.computerPick();

        if(this.isFinished()){
            System.out.println("Tu as perdu :(");
            return false;
        }
        this.displayRemaining();
        return true;
    }

    public static int add(int a, int b) throws Exception {
        int c = a + b;
        if (c == 0){
            throw new Exception("c is zeroooo !");
        }
        return c;
    }

    private void playerPick() {
        Scanner reader = new Scanner(System.in);
        System.out.println("Entrer un nombre entre 1 et 3");
        int n = reader.nextInt();
        while(!this.isUserInputValid(n)) {
            System.out.println("Entrer un nombre entre 1 et 3");
            n = reader.nextInt();
        }
        this.remaining -= n;
    }

    public boolean isUserInputValid(int n) {
        if(n < 1 || n > 3 || n >= this.remaining) {
            return false;
        }
        return true;
    }

    private void computerPick() {
        int n = new Random().nextInt(2) + 1;
        this.remaining -= n;
        System.out.println("Joueur2 prend " + n);
    }

    public boolean isFinished() {
        if(this.remaining == 1)
            return true;
        return false;
    }

    private void displayRemaining() {
        for(int i = 0; i < this.remaining; i++)
            System.out.print('|');
        System.out.println();
    }

    public void setRemaining(int r) {
        this.remaining = r;
    }
}
